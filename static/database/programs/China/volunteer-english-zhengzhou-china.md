# teaching english in high schools

## meta info

- meta info version: 0.1
- country: China
- city region: Zhengzhou
- starting year: 2015
- starting month: 0
- primary_category: volunteer
- duration: 6 months
- extendable: yes
- gallery: yes
- receiving partner: Xubo China
- minimum price: 1000 EUR
- language: English
- tags: middle school, english
- document created date: 2014-Oct-01
- document updated date: 2014-Oct-01

## program description

The school offers two placements, one is English teacher during two terms, another one is Summer Camp in July. During the term time, you will work with the students age from 13 to 18 years old. Each lesson lasts 45 minutes. You will work 10 to 15 lessons per week. During summer camp, the activities will be arranged according to where you are from. You can introduce your country’s culture, games and music, etc. Or you could just follow school’s schedule. We strongly suggest you to bring any typical teaching materials from your hometown.
### Accommodation

The volunteers will live in a flat in school campus or near campus. They will be provided with Internet service, air conditioners and hot water. 

### Meals

The three meals from Mondays to Fridays will be offered in the School cafeteria. If the school could not offer three meals volunteers will get food allowance instead. The volunteers can enjoy our Chinese character meals with our local students. Friendship will be cultivated via our meal time!!

### What our middle school can offer you

Have you ever been to the Shaolin Temple? Have you ever tried Chinese paper cut? Have you ever heard of the mysterious stories of Chinese traditional medicine? If not, we believe the exploration of the long history of Zhengzhou and its traditional and national features will be an adventure to you. The world natural and cultural heritage--- the Longmen Grottoes in Luoyang, The birthplace of Chinese kung fu---Shaoling Temple, The Chinese Mother River---The yellow River, the Oracle's Sites---Yin Xu in Anyang are the places you will not miss!