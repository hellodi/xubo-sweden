# teaching english at kindergartens

## meta info

- meta info version: 0.1
- country: China
- city region: Zhuji
- starting year: 2015
- starting month: 0
- primary_category: volunteer
- duration: 6 months
- extendable: yes
- gallery: yes
- receiving partner: Xubo China
- minimum price: 1000 EUR
- language: English
- tags: kindergarten, english
- document created date: 2014-Oct-01
- document updated date: 2014-Oct-01

## program description

Zhuji Creativity Kindergarten is located at No.17, North Zhan Jia Shan Rd, Tao Zhu Street, Zhuji, Zhejiang. It is a civilian-run kindergarten. The kids there are from 3 years old to 5 years old. There are 4 classes and 40-50 pupils in the kindergarten (each class has 8-10 pupils). There are 5 teachers in the kindergarten including one qulified American teacher who can share teaching experience with volunteers. It is a kindergarten especially for the pre-school kids. 

You will teach by your own or work with another volunteer in the class.
Your lessons will be a combination of teaching from the standard text books and
teaching lessons of your own design. Games always go down extremely well, too.

English: Very good

Working hours: 10 lessons to 15 lessons per week (20 to 25 minutes per lesson),
Monday to Friday. There are no classes neither in the evenings nor on Saturdays and Sundays.

Accommodation: school flat
