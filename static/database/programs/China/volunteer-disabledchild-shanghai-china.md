# help in disabled children centre 

## meta info

- meta info version: 0.1
- country: China
- city region: Shanghai
- starting year: 2015
- starting month: 0
- primary_category: volunteer
- duration: 6 months
- extendable: yes
- gallery: no
- receiving partner: Xubo China
- minimum price: 1000 EUR
- language: English
- tags: childer, care, disabled
- document created date: 2014-Oct-10
- document updated date: 2014-Oct-10

## program description

Disabled center in Shang Gang new residential quarters is a center for the mentally handicapped in Shang Gang village. There are around 20 students in the center. They are aged from 16 to 35 years old. That center is run by an organization called 'Shanghai Enrichment Community Service Centre'. The motto of the center is 'Sunshine·Charity'. The center is a comprehensive center equipped with sunshine base(Vocational rehabilitation aid base ), sunny garden( care center for the disabled during the daytime), sunshine center. Also they have a charity supermarket in order to offer the jobs for the disabled and also aids them. 

What you can do as a volunteer:

- Helping the students to look after themselves
- Helping the students to learn basic English
- Helping the staff at the centre to design the curriculum
- You can also get involved behind the scenes, designing the center’s website, updating the information and writing reports and articles for it.

Minimum: 2 Weeks
English: no requirement
Working hours: 8 hours per day from Mon. to Fri. 
Travel time: from volunteer flat to the centre, 40 minutes 