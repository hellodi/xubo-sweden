# teach english at kindergartens

## meta info

- meta info version: 0.1
- country: China
- city region: Shanghai
- starting year: 2015
- starting month: 0
- primary_category: volunteer
- duration: 6 months
- extendable: yes
- gallery: yes
- receiving partner: Xubo China
- minimum price: 1000 EUR
- language: English
- tags: high school, kindergarten, english
- document created date: 2014-Oct-01
- document updated date: 2014-Oct-01

## program description

You will teach by your own or work with another volunteer in the class. Your lessons will be a combination of teaching from the standard text books and teaching lessons of your own design, which can be anything from your hobbies or interests to current affairs and local news as subject matters. Games always go down extremely well, too.

The vast majority of your teaching will focus on improving the students' conversational language, familiarizing them with correct pronunciation and boosting their confidence. They’re often exemplary at grammars of a language but have such little practice in spoken English, German or French that they make many mistakes. This is why you come in!
English: Very good

Working hours: 10 lessons to 15 lessons per week (35 to 40 minutes per lesson in Elementary school, Middle school and High school; 20 to 25 minutes per lesson in the kindergarten), Monday to Friday. There are no classes neither in the evenings nor on Saturdays and Sundays.

Accommodation: school flat or our volunteer flat 
Age of Children in the kindergarten: 2 to 6 years old 
Age of Primary school students: 7 to 12 years old 
Age of Secondary school students: 13 to 16 years old.
Age of High school students: 16 to 19 years old 

From the end of Feb to the end of May, from 1 Sept to the end of December, the kindergartens, primary schools, secondary schools and high schools are open to all volunteers. From the beginning of June to the end of June and the beginning of Jan to the one week before Chinese New Year, only Kindergarten and disabled center open to volunteers due to the exam month. In summer holiday, July and Aug, we do offer camps for the volunteers. The camps are either organized by government school or the local language schools. 