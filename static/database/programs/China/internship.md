# Internship Program

## Meta Info

- meta info version: 0.1
- country: China
- city region: Shanghai
- starting year: 2015
- starting month: 1, 2, 3, 12
- primary category: internship
- duration: 6 months
- extendable: yes
- gallery: yes
- receiving partner: Xubo China
- minimum price: 1000 EUR
- language: English
- tags: internship, architecture, marketing, sales, nursery, logistic, travel, consultant, hospitality
- document created date: 2014-Oct-24
- document updated date: 2014-Oct-24

## Program Description

_Type One: Program fee with airport picking up and accommodation:_

**When you transfer the program and accommodation fee, please add bank commission from Singapore side which is Bank commission: 20 Singapore dollars + 0.128% of total transfer**

The accommodations are in service apartments in Shanghai. There are three different locations. The room offers air condition, 3 to 4 people shared bathroom, toilet and hot water, wifi.

The Internship mediation contains:

- Mediation in internship
- Picking up from the airport
- Induction course
- 24 hours support
- One culture course
- Foreign living registration process
- Placement Cancellation Fee: 100 Euro if the intern cancels the confirmed
placement.

Exclude:

- International flights
- Insurance
- Electricity Bills monthly (according to the actual bills each month)
- Transport fee to the placement
- Meals
- Pocket money

Accommodation condition:

- The accommodation booking confirmation has to be one month before arrival.
- Cancellation fee of 250 Euro will be charged if the accommodation has been
confirmed.
- The interns will sign the accommodation rules and room products checklist
when they arrival. See the sample of rules and checklist enclosed.
- The interns will pay deposit of 1600 RMB to the apartment owner upon arrival.
If anything damage when he/she leaves the apartment, the interns agree to pay
the certain fee from the deposit according to the paper of “Settlement Charges
Guide”

Booking period: Please send the application forms 4 month before the placement
starts.

Sample of first three days:

Day One: Airport picking up by our contract driver and settle down in the service apartment in Shanghai.

Day Two: The intern will take metro to Xu Bo office for induction course and culture course.

Day Three: The intern will go to the company by him/herself. In Day Two induction course, our program coordinator will tell the interns how to go to the company and what time she/he supposes to arrive there and whom our intern will meet up at the office.

_Type Two: Program fee without airport picking up and accommodation_

If the student does not take the accommodation, the fee for Xu Bo is EUR 510.

The price includes
- Internship placement
- Airport picking up arrangement exclude the fare*
- Induction course**
- 24/7 support
- One culture course**

* We can help them to book the airport picking up service with our contract driver. The interns will pay the taxi fare to the taxi driver directly.
** Interns need to arrive at our office either Tuesdays or Thursdays apart from public holidays for induction course. Please confirm with us the induction course date and Culture course date before booking the flight ticket.

Cancellation Fee: 100 Euro if the intern cancels the confirmed placement.

Sample of first three days:
Day One (should be Tuesdays or Thursdays apart from public holidays): The intern will take metro to Xu Bo office for induction course and culture course
Day Two: The intern will go to the company by him/herself. (In Day Two induction course, our program coordinator will tell the interns how to go to the company and what time she/he supposes to arrive there and whom our intern will meet up at the office.)

Cancellation Policy:
- Cancellation after acceptance and prior to placement confirmation - the deposit is forfeited.
- Cancellation after confirmation of placement – 100 Euro or 200 US dollars
- No Show or Early Departure from Program - 100% of the program fee is forfeited.

About Visa:
All interns will apply for M visa or F visa coming to China.

