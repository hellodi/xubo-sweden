import os
from datetime import datetime
import time
from time import mktime
import pymongo
from pymongo import MongoClient
import re

print "parsing the programs data..."

# Grab our connection information from the MONGOHQ_URL environment variable
# (mongodb://linus.compose.io:10045 -u username -pmy_password)
MONGO_URL = "mongodb://jd:12345678@linus.mongohq.com:10010/xubo_database"
#MONGO_URL = "mongodb://'tester':'password'@ds033400.mongolab.com:33400/bnsl-fika-db"
#connection = Connection(MONGO_URL)
client = MongoClient(MONGO_URL)
 
# Specify the database
db = client.xubo_database

def list_md_files(dir):                                                                                                  
    r = []                                                                                                            
    subdirs = [x[0] for x in os.walk(dir)]                                                                            
    for subdir in subdirs:                                                                                            
        files = os.walk(subdir).next()[2]                                                                             
        if (len(files) > 0):                                                                                          
            for file in files:
            	if file.endswith('.md'):                                                                                 
                	r.append(subdir + "/" + file)
    r.sort()                                                                       
    return r

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def is_float(x):
	try:
		a = float(x)
	except ValueError:
		return False
	else:
		return True

def is_int(x):
	try:
		a = float(x)
		b = int(a)
	except ValueError:
		return False
	else:
		return a == b

def parse(filename):
	### Sync the programs collection ###
	file = open(filename, 'r')
	all_programs = []
	new_program = {}
	this_description = ""
	description_flag = False
	meta_flag_set = False

	for line in file:
		if not description_flag == True:
			line = line.lower()
		# look for program name
		program_name = re.match("^# (.*$)", line) # matchi anything until the end of line
		if program_name:
			print 'Found program name: ' + program_name.group(1)
			new_program['name'] = program_name.group(1)
			continue

		# look for meta info
		if re.match("## meta info", line):
			print 'Found meta info section. Meta flag on. Description flag off.'
			meta_flag_set = True
			description_flag = False
			continue

		# look for program description
		if re.match("## program description", line):
			print 'Found program description section. Description flag on, meta flag off.'
			meta_flag_set = False
			description_flag = True
			continue

		# analyze meta info
		meta_info = re.match("^- (.*$)", line)
		if meta_info and meta_flag_set:
			key_value_string = re.match("(^.*):(.*$)", meta_info.group(1))
			print 'Decoding meta info...'
			keyword = key_value_string.group(1).strip().replace(' ', '_')
			
			temp_values = key_value_string.group(2).strip().split(',') # slip the value part
			value_array = [] # used if the values are array
			key_value_pair = {} # to store the final key pair decoded

			if len(temp_values) > 1: # if is list value
				for i in range(len(temp_values)):
					temp_values[i] = temp_values[i].strip()
					if is_int(temp_values[i]): # convert int value string to int
						temp_values[i] = int(temp_values[i])
					elif is_float(temp_values[i]): # convert float value string to float
						temp_values[i] = float(temp_values[i])

					value_array.append(temp_values[i])
				# asign the array to the final key pair
				key_value_pair[keyword] = value_array

			else: # if is single value
				single_value = temp_values[0].strip()
				if is_int(single_value): # convert int value string to int
					single_value = int(single_value)
				elif is_float(single_value): # convert float value string to float
					single_value = float(single_value)
				# asign the value to the final key pair
				key_value_pair[keyword] = single_value

			print key_value_pair
			new_program.update(key_value_pair)
			continue

		if description_flag == True:
			this_description = this_description + line

	# at the end of line, asigne final description, convert time string to time,
	new_program["description"] = this_description
	new_program['program_URI'] = new_program['name'].replace(' ', '-') + '-' +\
							     new_program['country'].replace(' ', '-') + '-' +\
	                             new_program['city_region'].replace(' ', '-') + '-' +\
	                             new_program['primary_category'] + '-' +\
	                             str(new_program['starting_year']) + '-' +\
	                             new_program['document_created_date']
	new_program['document_updated_date'] = datetime.fromtimestamp(mktime(time.strptime(new_program['document_updated_date'], "%Y-%b-%d")))
	new_program['document_created_date'] = datetime.fromtimestamp(mktime(time.strptime(new_program['document_created_date'], "%Y-%b-%d"))) 

	# return the parsed data
	return new_program

db.programs.remove( { } )
all_md_files = list_md_files(".")
for one_md_file  in all_md_files:
	program = parse(one_md_file)
	db.programs.insert(program)
	print 'Finished one program \n\n'