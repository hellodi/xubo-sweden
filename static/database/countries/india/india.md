# India

## meta info

- meta info version: 0.1
- continent: Asia
- capital: New Delhi
- currency: Indian Rupee (INR)
- population: 1.2 billion
- language: Hindi, English
- time zone: UTC +5.30
- wiki: en.wikipedia.org/wiki/India
- gallery: yes
- receiving partners: Volunteer Solutions
- tags: population, south asia, history
- document created date: 2014-Nov-09
- document updated date: 2014-Nov-09

## country description

China is a dynamic, diverse and stimulating city – the very epitome of modern China. Though Shanghai cannot rival Beijing in scenery or cultural heritage, its varied architectural styles and cosmopolitan feel give it a charm of its own. Shanghai covers a total area of 6,341 square kilometers with a population of 25,000,000. The average temperature is around 15.8 degree Celsius and the annual precipitation 1,240 millimeters. Within its jurisdiction are 16 districts and one county.