import os
from datetime import datetime
import time
from time import mktime
import pymongo
from pymongo import MongoClient
import re
import cloudinary.api
import cloudinary
import cloudinary.uploader
from cloudinary.utils import cloudinary_url
import webbrowser

# cloudinary config
cloudinary.config(
  cloud_name = 'xubo-sweden',  
  api_key = '148369738863661',  
  api_secret = 'PA6dmUyTBx_44mru9z8CLDYnZKs'  
)

# Grab our connection information from the MONGOHQ_URL environment variable
# (mongodb://linus.compose.io:10045 -u username -pmy_password)
MONGO_URL = "mongodb://jd:12345678@linus.mongohq.com:10010/xubo_database"
#MONGO_URL = "mongodb://'tester':'password'@ds033400.mongolab.com:33400/bnsl-fika-db"
#connection = Connection(MONGO_URL)
client = MongoClient(MONGO_URL)
 
# Specify the database
db = client.xubo_database

# list all files in the 'dir' with 'file_type'
# example: list_files('.', '.md')
def list_files(dir, file_type):                                                                                             
    file_path = []
    dir_path = []                                                                                                      
    subdirs = [x[0] for x in os.walk(dir)]                                                                            
    for subdir in subdirs:                                                                                            
        files = os.walk(subdir).next()[2]                                                                             
        if (len(files) > 0):                                                                                          
            for one_file in files:
            	if one_file.endswith(file_type):                                                                                 
                	file_path.append({'dir': subdir + "/", 'filename': one_file})
    file_path.sort()
    return file_path

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def is_float(x):
	try:
		a = float(x)
	except ValueError:
		return False
	else:
		return True

def is_int(x):
	try:
		a = float(x)
		b = int(a)
	except ValueError:
		return False
	else:
		return a == b

def parse(filename):
	### Sync the countries collection ###
	file = open(filename, 'r')
	all_countries = []
	new_country = {}
	this_description = ""
	description_flag = False
	meta_flag_set = False

	for line in file:
		if not description_flag == True:
			line = line.lower()
		# look for country name
		country_name = re.match("^# (.*$)", line) # matchi anything until the end of line
		if country_name:
			print 'Found country name: ' + country_name.group(1)
			new_country['name'] = country_name.group(1)
			continue

		# look for meta info
		if re.match("## meta info", line):
			print 'Found meta info section. Meta flag on. Description flag off.'
			meta_flag_set = True
			description_flag = False
			continue

		# look for country description
		if re.match("## country description", line):
			print 'Found country description section. Description flag on, meta flag off.'
			meta_flag_set = False
			description_flag = True
			continue

		# analyze meta info
		meta_info = re.match("^- (.*$)", line)
		if meta_info and meta_flag_set:
			key_value_string = re.match("(^.*):(.*$)", meta_info.group(1))
			print 'Decoding meta info...'
			keyword = key_value_string.group(1).strip().replace(' ', '_')
			
			temp_values = key_value_string.group(2).strip().split(',') # slip the value part
			value_array = [] # used if the values are array
			key_value_pair = {} # to store the final key pair decoded

			if len(temp_values) > 1: # if is list value
				for i in range(len(temp_values)):
					temp_values[i] = temp_values[i].strip()
					if is_int(temp_values[i]): # convert int value string to int
						temp_values[i] = int(temp_values[i])
					elif is_float(temp_values[i]): # convert float value string to float
						temp_values[i] = float(temp_values[i])

					value_array.append(temp_values[i])
				# asign the array to the final key pair
				key_value_pair[keyword] = value_array

			else: # if is single value
				single_value = temp_values[0].strip()
				if is_int(single_value): # convert int value string to int
					single_value = int(single_value)
				elif is_float(single_value): # convert float value string to float
					single_value = float(single_value)
				# asign the value to the final key pair
				key_value_pair[keyword] = single_value

			print key_value_pair
			new_country.update(key_value_pair)
			continue

		if description_flag == True:
			this_description = this_description + line

	# at the end of line, asigne final description, convert time string to time,
	new_country["description"] = this_description
	new_country['country_URI'] = new_country['continent'].replace(' ', '-') + '-' +\
						   new_country['name'].replace(' ', '-')

	new_country['document_updated_date'] = datetime.fromtimestamp(mktime(time.strptime(new_country['document_updated_date'], "%Y-%b-%d")))
	new_country['document_created_date'] = datetime.fromtimestamp(mktime(time.strptime(new_country['document_created_date'], "%Y-%b-%d"))) 

	new_country['wiki'] = 'http://' + new_country['wiki']


	# return the parsed data
	return new_country

def start_sync():
	all_md_files = list_files('.', '.md')
	for one_md_file  in all_md_files:
		print 'found file: '
		print one_md_file
		country = parse(one_md_file['dir'] + one_md_file['filename'])

		# search jpg files and upload to cloudinary
		print 'Searching for jpg in folder: '
		all_jpg_files = list_files(one_md_file['dir'], '.jpg')
		country_images_in_cloud = cloudinary.api.resources_by_tag(country['name'].replace(' ', '-'))

		for one_jpg_file in all_jpg_files:
			cloudinary_public_id = 'site-static/countries/' + country['country_URI'] + '/' + one_jpg_file['filename'].replace('.jpg', '')
			cloudinary_folder = 'site-static/countries/' + country['country_URI']


			image_already_uploaded_flag = False

			# check if this image exists in the cloud
			for one_image_in_cloud in country_images_in_cloud['resources']:
				if one_jpg_file['filename'].replace('.jpg', '') in one_image_in_cloud['public_id']:
					image_already_uploaded_flag = True
					print "This image has already been uploaed before - skip this time."

			if image_already_uploaded_flag == False:
				print 'Upload image...' + cloudinary_public_id
				
				# put the thumb tag on files with thumb in name
				if 'thumb' in one_jpg_file['filename']:
					image_tags = [country['name'], 'thumb', 'country-image']
				else:
					image_tags = [country['name'], 'country-image']

				response = cloudinary.uploader.upload(one_jpg_file['dir']+one_jpg_file['filename'], \
										   folder = cloudinary_folder, \
										   use_filename = True,
										   tags = image_tags)

		# insert into mongodb
		db.countries.insert(country)
		print 'Finished one country \n\n'

print "Syncing the countries data..."

db.countries.remove( { } )
start_sync()

