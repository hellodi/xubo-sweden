# Important!

The cities, programs and participants all have a unique resource identifier: URI. The URI is generated using the meta data of these models by the python sync scripts. The folder names and file names are set according to their URI. Therefore, ** Don't change the naming scheme of the folders and files unless you know what you are doing! **