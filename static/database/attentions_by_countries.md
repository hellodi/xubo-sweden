- Materials to bring
Most types of teaching materials will be welcome. We also encourage volunteers to bring photos and maps etc. of their country, hometown, house, or family etc. Students are curious about that kind of information and it’s easy to make conversations around them.

- Clothing
The dress code is smart casual but no low collared tops or tops that show your stomach please! The school also specifies no hair dye, visible piercing, shorts or short skits.

- Accommodation details
You will live in a shared room in our flat or the school accommodation. The
flat has an air conditioner, a washing machine(either at compus or outside
campus) and hot shower. Some flats have internet access and a kitchen. 

- Phones, Banks, Post
If you have a mobile phone, you can buy a Chinese Sim Card for around 10
USD, but you could not call overseas. But you can buy an International Phone
Card to call overseas, which only costs 6 USD and lasts 25 minutes to call
overseas. But please make sure to consult the telecom carrier and unlock the
phone before coming to China, so the phone could match to Chinese Sim Card.
The ICBC(Commercial and Industrial Bank of China) is nearby, where you
may use ATM-machines taking Visa and Master Cards. Banks are open from
Monday to Friday 9.00.a.m. to 5.00 p.m. except on Sundays. The Post office is
near, you can send letters or parcels. Post office open 9 a.m. to 5 p.m.from
Monday to Friday.

- Water Safety
Although water purity in China is improving, its quality varies throughout the city. Most expatriates use bottled distilled water for their household needs,
which is available in 5 gallon ("water cooler") sized containers from several
retailers who will deliver. In the office area, water containers are always
provided. Please remember that in China people always use distilled water for
drinking and ice cubes, and when cooking anything which absorbs water, such
as pasta or rice.

- Hospitals & Doctors
There is a hospital nearby our schools. The hospital offers a walk-in service,
where you can see a general practitioner. There are also a lot of pharmacies
however if you rely on special medication we recommend that you bring it
from home.

- Visa
You should obtain a 30-Day to 90-Day Class M/F Visa in order to enter the
country. If your visa length can not cover whole placement period, you must
go to Hong Kong to get extra visa stay. All cost to Hong Kong will be your
own.

- Cost
2 weeks: 359 Euro (Only for the placement in kindergarten or disable children
centre)
3 weeks: 459 Euro (Only for the placement in kindergarten or disable children
centre)
4 weeks: 559 Euro
5 weeks: 659 Euro
6 weeks: 759 Euro
7 weeks: 799 Euro
8 weeks: 876 Euro
9 Weeks: 949 Euro
10 weeks: 1,019 Euro
11 weeks: 1,089 Euro
12 weeks:1,159 Euro
13 Weeks: 1,235 Euro

Cancellation Policy: 100 Euro will be charged after the placement has been
confirmed. No refund after volunteers arrival at China.

All fees included
1. Orientation upon arrival
2. 24-hour support
3. Accommodation
4. Airport Pickup/Hotel airport
5. Regular follow-up
6. Food (Monday to Friday at school time)
7. Teaching introduction
8. 2 days orientation (includes, induction course, 1-hour culture course, without
food- 1 Meal 10 to 30 RMB)
9. Donation to poor schools in Si Chuan Province

The arrival date is on Mondays or Wednesdays (See the sheet of
Holidays and start dates in 2015). Our picking up time is from
8am to 5pm, Mondays and Wednesdays. If volunteers arrive at
Shanghai one or more day earlier , she/he would book their own
hotel. Xu Bo staff will pick up volunteers at hotel on Mondays or
Wednesdays. If they will not arrive earlier than 5pm, she/he will
book her/his own hotel that night and Xu Bo staff will pick up at
hotel the next day. Volunteers will afford their own hotel fee and
transport from airport to the hotel. Xu Bo staff can help
volunteers to reserve the hotel which is nearby the office.
