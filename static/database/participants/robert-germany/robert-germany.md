# Robert

## participant meta info

- meta info version: 0.1
- country: Germany
- city region: N/A
- thumb id: site-static/participants/robert-germany/robert-germany-thumb
- email: robgoral@web.de
- twitter: N/A
- facebook: N/A
- blog: N/A
- linkedin: N/A
- phone: N/A
- tags: internship
- document created date: 2014-Oct-26
- document updated date: 2014-Oct-26
- include in search: true

## story meta info

- story name: teaching english at kindergarten
- story album id: site-static/participants/robert-germany/robert-germany-2014-10-01
- visit country: China
- visit city: Shanghai
- start date: 2011-Jun-01
- end date: 2011-Sep-30
- program primary category: volunteer
- receving org: xubo china
- tags: volunteer, teaching, english, kindergarten
- gallery: true
- include in search: true

## story text
Hi, my name is Robert, I come from Germany and I've been a volunteer in a Shanghai kindergarten for 8 weeks, and boy do I miss that city! It has the highest population of all Chinese cities and has also the highest amount of skyscrapers. And indeed, the view over the Pudong skyline when standing on the Bund (near Huangpu river) is breathtaking, something I will never forget my whole life. In summer, it's really really hot outside, but that isn't much of a problem since there's an air conditioner in almost every room of Chinese buildings.

The kindergarten itself was very nice. It had great equipment, was very clean and a very friendly staff, always willing to help me out with whatever problems I had. Of course, the best part were the cute children. Every week I got another class, and they were all incredibly curious and happy to see the foreign volunteers. Every day I held some English lessons with them. Since they were young (between 3-6), I had to come up with games that would motivate them to speak English. It went very well, the children were highly motivated and had lots of fun. There is also always some teacher around you to give support if it is needed. 

On hot days, we would go out into the schoolyard and have waterfights with waterguns. At the end of the week, we organized big events in which we would introduce the children to the culture of our homecountries, by playing games with them. For example, on „Germany Day“, we let them make clay Bretzels. They are very curious about foreigners and ask all kind of questions. I even learned some Chinese from them :-) 

In the break time (when the children are sleeping) I had lunch with the other volunteers and we planned future lessons or where to go in the afternoon. 

In my free time I often went into the city. There's so much to see, after 8 weeks I still had the impression that I still haven't seen nearly enough. Regardless if following some travel guide or just go to a random direction, there would always be something impressive. There's an endless amount of entertaining opportunities, be it iceskating in one of the large stores, have a massage in a massage parlor, watch some chinese movie in the giant IMAX cinema, go into the beautiful museums, or witness the stunning view of Shanghai from the 88th floor of the Jinmao tower (formerly the highest skyscraper, but now there's the even taller World Financial Centre). The food is also great! Even if you don't want to eat something exotic like frog, there are many very tasty dishes (I personally recommend „hotpot“). 

One weekend, I travelled to "Huang Shan" (Yellow Mountain), which was 5 hours by bus. A big contrast to the metropolis, the Yellow Mountain is one of the most beautiful landscapes I've ever seen. As I climbed up the endless stone steps, one magnificent view after another would unfold. Though, I missed the last cable car into the valley, because I totally forgot the time, so I had to check in into one of the hotels on the mountaintops. I spent the night with 7 Chinese men in a small room, watching a game of women soccer on TV, having great fun. Chinese people are so heartwarming and friendly, and although none the Chinese guys spoke English, we communicated with body language, and they just wouldn't stop to give me drinks and cigarettes, although I desperately tried to tell them that I don't smoke! :-) 

The 9th week, I went for a trip to Beijing to visit a friend there. It was a fun experience to dive in a overnight train, with it's small but comfortable (and cheap!) beds. Beijing itself is also a marvelous city! I visited Tiananmen Square and the Forbidden City, and went wandering on the Great Wall (which was definitely a highlight). 

I had a great time in China with the other volunteers. Our volunteers' flat was very nice, and the friendly Xu Bo staff is just the best thing that could happen to you if you seek introduction to this exciting country. I actually liked it so much, that I decided to study Chinese afterwards, something I didn't regret to the present day. I will go back to China with my class in a couple of weeks, having language lessons in Changchun. Can't wait to return! 

To see a video of my experience in China, click here. Best wishes and go for it!

----------