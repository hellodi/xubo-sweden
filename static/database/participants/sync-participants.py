import os
from datetime import datetime
import time
from time import mktime
import pymongo
from pymongo import MongoClient
import re
import cloudinary.api
import cloudinary
import cloudinary.uploader
from cloudinary.utils import cloudinary_url

print "Syncing the participants data..."

# cloudinary config
cloudinary.config(
  cloud_name = 'xubo-sweden',  
  api_key = '148369738863661',  
  api_secret = 'PA6dmUyTBx_44mru9z8CLDYnZKs'  
)

# Grab our connection information from the MONGOHQ_URL environment variable
# (mongodb://linus.compose.io:10045 -u username -pmy_password)
MONGO_URL = "mongodb://jd:12345678@linus.mongohq.com:10010/xubo_database"
#MONGO_URL = "mongodb://'tester':'password'@ds033400.mongolab.com:33400/bnsl-fika-db"
#connection = Connection(MONGO_URL)
client = MongoClient(MONGO_URL)
 
# Specify the database
db = client.xubo_database

def list_md_files(dir):                                                                                                  
    r = []                                                                                                            
    subdirs = [x[0] for x in os.walk(dir)]                                                                            
    for subdir in subdirs:                                                                                            
        files = os.walk(subdir).next()[2]                                                                             
        if (len(files) > 0):                                                                                          
            for file in files:
                if file.endswith('.md'):                                                                                 
                    r.append(subdir + "/" + file)
    r.sort()                                                                       
    return r

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def is_float(x):
    try:
        a = float(x)
    except ValueError:
        return False
    else:
        return True

def is_int(x):
    try:
        a = float(x)
        b = int(a)
    except ValueError:
        return False
    else:
        return a == b

def parse(file_name):
    ### parse participants collection ###
    print file_name
    file = open(file_name, 'r')
    new_participant = {}
    new_story = {}
    this_story_text = ""
    story_text_flag = False
    participant_meta_flag_set = False
    story_meta_flag_set = False
    new_participant['stories'] = []

    for line in file:
        if not story_text_flag == True:
            line = line.lower()
        # look for participant name
        participant_name = re.match("^# (.*$)", line) # matchi anything until the end of line
        if participant_name:
            new_participant['name'] = participant_name.group(1)
            print new_participant['name']
            continue

        # look for participant meta info
        if re.match("## participant meta info", line):
            participant_meta_flag_set = True
            story_meta_flag_set = False
            story_text_flag = False
            continue

        # look for story meta info
        if re.match("## story meta info", line):
            story_text_flag = False
            participant_meta_flag_set = False
            story_meta_flag_set = True
            continue

        # look for participant story_text
        if re.match("## story text", line):
            story_text_flag = True
            participant_meta_flag_set = False
            story_meta_flag_set = False
            continue

        meta_info = re.match("^- (.*$)", line)
        if meta_info:
            key_value_string = re.match("(^.*):(.*$)", meta_info.group(1))
            print 'Decoding meta info...'
            keyword = key_value_string.group(1).strip().replace(' ', '_')
            
            temp_values = key_value_string.group(2).strip().split(',') # slip the value part
            value_array = [] # used if the values are array
            key_value_pair = {} # to store the final key pair decoded

            if len(temp_values) > 1: # if is list value
                for i in range(len(temp_values)):
                    temp_values[i] = temp_values[i].strip()
                    if is_int(temp_values[i]): # convert int value string to int
                        temp_values[i] = int(temp_values[i])
                    elif is_float(temp_values[i]): # convert float value string to float
                        temp_values[i] = float(temp_values[i])

                    value_array.append(temp_values[i])
                # asign the array to the final key pair
                key_value_pair[keyword] = value_array

            else: # if is single value
                single_value = temp_values[0].strip()
                if is_int(single_value): # convert int value string to int
                    single_value = int(single_value)
                elif is_float(single_value): # convert float value string to float
                    single_value = float(single_value)
                # asign the value to the final key pair
                key_value_pair[keyword] = single_value

            print key_value_pair
            if participant_meta_flag_set == True:
                new_participant.update(key_value_pair)
            elif story_meta_flag_set == True:
                new_story.update(key_value_pair)
            continue

        if re.match("^----------", line):
            new_story["story_text"] = this_story_text
            new_participant['stories'].append(new_story)
            url, options = cloudinary_url(new_story['story_album_id'], format = 'jpg')
            new_story['story_album_path'] = url
            this_story_text = ""
            story_text_flag = False
            continue

        if story_text_flag == True:
            this_story_text = this_story_text + line
            continue

    new_participant['participant_URI'] = new_participant['name'].replace(' ', '-') + '-' +\
                                         new_participant['country'].replace(' ', '-')

    url, options = cloudinary_url(new_participant['thumb_id'],
                format = 'jpg',
                width = 150,
                height = 150,
                crop = "thumb",
            )
    new_participant['thumb_150_path'] = url

    url, options = cloudinary_url(new_participant['thumb_id'],
                format = 'jpg',
                width = 100,
                height = 100,
                crop = "thumb",
            )
    new_participant['thumb_100_path'] = url

    new_participant['document_updated_date'] = datetime.fromtimestamp(mktime(time.strptime(new_participant['document_updated_date'], "%Y-%b-%d")))
    new_participant['document_created_date'] = datetime.fromtimestamp(mktime(time.strptime(new_participant['document_created_date'], "%Y-%b-%d"))) 
                                
    return new_participant                              


db.participants.remove( { } )
all_md_files = list_md_files(".")
for one_md_file  in all_md_files:
    participant = parse(one_md_file)
    db.participants.insert(participant)
    print 'Finished one program \n\n'

