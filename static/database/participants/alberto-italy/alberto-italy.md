# Alberto

## participant meta info

- meta info version: 0.1
- country: Italy
- city region: N/A
- thumb id: site-static/participants/alberto-italy/alberto-italy-thumb
- email: N/A
- twitter: N/A
- facebook: N/A
- blog: N/A
- linkedin: N/A
- phone: N/A
- tags: internship, architecture, marketing, sales, nursery, logistic, travel, consultant, hospitality
- document created date: 2014-Oct-26
- document updated date: 2014-Oct-26
- include in search: true

## story meta info

- story name: teaching english in kindergarten
- story album id: site-static/participants/alberto-italy/alberto-1
- visit country: china
- visit city: shanghai
- departure date: 2011-Jul-01
- return date: 2011-Sep-30
- program primary category: volunteer
- receving_org: xubo china
- tags: [volunteer, teaching, kindergarten, english]
- gallery: True
- include in search: true

## story text
I am Alberto, a seventeen years old guy from Italy; six months ago, my parents suggested me to do a work experience somewhere in the world, I’ve always been interested in Asian and, in a particular way, in the Chinese culture, so I decided to become a volunteer for six weeks in Shanghai. 

Before the travel I was nervous, scared and I was regretting that choice: I thought that I would be alone, that people would not understand me if I have tryed to communicate and that I wouldn’t spend good time, but already after the first day of the program I understood that I have never been that wrong. 

The roommates were nice and very friendly and I created, with them, relationships that will continue also after the staying in Shanghai, together we travelled around Shanghai, we tasted new food, we met local people... Basically we discovered a culture and a lifestyle completely different from ours. 

The work in the kindergarten was very interesting and instructive. I was surrounded by nice people who were professional, ready to help, and able to make you feel good in every situation. I had a lot of fun with the kids, they were all curious and ready to learn new things; we used to teach them the English language with games and songs and we also made some funny activities. The work took only the morning so I was able to travel and discover as much as I can about Shanghai and China for the rest of the day. 

The people who work at Xu Bo are an important landmark you can always trust in, very well prepared in everything and always friendly and kind. I fell in love with China and Chinese and I promised myself to come back here for another experience. 

I am sure that once I go back to Itlay I will miss this city and I will be bored of everyday life. I met great people, discovered amazing places, lived fantastic experiences. Those six weeks has passed extremely quickly and I will never forget this wonderful experience.

----------