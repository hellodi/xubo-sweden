# Singapore

## meta info

- meta info version: 0.1
- country: Singapore
- lattitude: 1.283333
- longitude: 103.833333 
- airport: Singapore Changi Airport (SIN)
- currency: Singapore dollar (SGD)
- population: 5.5 million
- language: English, Malay, Mandarin, Tamil
- time zone: UTC +8
- wiki: en.wikipedia.org/wiki/Singapore
- gallery: yes
- receiving partners: Xubo Singapore
- tags: south east asia, small, harbor, capital
- document created date: 2014-Oct-20
- document updated date: 2014-Oct-20

## city description

Singapore (Listeni/ˈsɪŋəpɔr/ or /ˈsɪŋɡəpɔr/), officially the Republic of Singapore, is a sovereign city-state and island country in Southeast Asia. It lies off the southern tip of the Malay Peninsula and is 137 kilometres (85 mi) north of the equator. The country's territory consists of the lozenge-shaped main island, commonly referred to as Singapore Island in English and Pulau Ujong in Malay, and more than 60 significantly smaller islets. Singapore is separated from Peninsular Malaysia by the Straits of Johor to the north, and from Indonesia's Riau Islands by the Singapore Strait to the south. The country is highly urbanised, and little of the original vegetation remains. The country's territory has consistently expanded through land reclamation.