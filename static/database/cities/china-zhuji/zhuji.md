# zhuji

## meta info

- meta info version: 0.1
- country: China
- lattitude: 29.7166
- longitude: 120.2333
- airport: N/A
- currency: Renminbi (RMB)
- population: 1.07 million
- language: Chinese
- time zone: UTC +8
- wiki: en.wikipedia.org/wiki/Zhuji
- gallery: yes
- receiving partners: Xubo China
- tags: small town
- document created date: 2014-Oct-20
- document updated date: 2014-Oct-20

## city description
Zhuji City is one of the ancient capitals of Yue Kingdom and the native place of Xishi.It covers an area of 2,311 square kilometers with a population of 1.0559 million. Locating in the south wing of the Yangtze Delta, middle and north part of Zhejiang, with only 200 km away from Shanghai. Zhuji enjoys an obvious regional advantage. With picturesque mountains and rivers, Zhuji is one of the China’s excellent tourist cities, in which there is a State-level Scenic Region -- Huanjiang River/Wuxie Scenic Region. 

Zhuji is in the subtropical monsoon climate region, with clearly demarcated four seasons and comfortable climate, annual average temperature of 16.2 degree C, annual average rainfall of 1315.9 mm, and nonfrost period of about 234 days.