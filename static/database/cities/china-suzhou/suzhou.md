# suzhou

## meta info

- meta info version: 0.1
- country: China
- lattitude: 31.2989
- longitude: 120.5853
- airport: N/A
- currency: Renminbi (RMB)
- population: 1.3 million
- language: Chinese
- time zone: UTC +8
- wiki: en.wikipedia.org/wiki/Suzhou
- gallery: yes
- receiving partners: Xubo China
- tags: casual life, landscape, history, industry
- document created date: 2014-Oct-02
- document updated date: 2014-Oct-02

## city description
A city of historic culture and renowned tourist attraction, Suzhou is located in the southeast part of Jiangsu Province right in the middle of the Yangtze River Delta, in a coastland economically developed region. The city of Suzhou, which has a history of 2,500 years, was first planned in 514 BC. Today the city proper, which was laid out in the "double-chessboard" form, still maintains its ancient features. Canals seen in the city serve as navigable waterways along with the land transportation. Civilian houses built on streets have their backsides facing man-made waterways. Visitors to Suzhou are greatly impressed by the bridges, waters, the white-painted walls and the darkish roof tiled houses of a traditional style, and by the elegance of the classical gardens in the city. The historical and cultural heritage of Suzhou, and the beauty of its classical gardens have made the city, as is called in China, “Paradise on Earth”. 

Situated in a Temperate Zone, Suzhou enjoys a favorable geographical and ecological environment. The climate in this area is generally mild, marked with distinct changes of the four seasons. The average temperature in the year is 17.4°C and the annual rainfall over 1100 millimeters.
