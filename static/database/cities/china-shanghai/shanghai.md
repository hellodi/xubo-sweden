# shanghai

## meta info

- meta info version: 0.1
- country: China
- lattitude: 31.2304
- longitude: 121.4737
- airport: Shanghai Pudong International Airport (PVG)
- currency: Renminbi (RMB)
- population: 23 million
- language: Chinese
- time zone: UTC +8
- wiki: en.wikipedia.org/wiki/Shanghai
- gallery: yes
- receiving partners: Xubo China
- tags: finance, economic center, history, colony
- document created date: 2014-Oct-02
- document updated date: 2014-Oct-12

## city description
Shanghai is a dynamic, diverse and stimulating city – the very epitome of modern China. Though Shanghai cannot rival Beijing in scenery or cultural heritage, its varied architectural styles and cosmopolitan feel give it a charm of its own. Shanghai covers a total area of 6,341 square kilometers with a population of 25,000,000. The average temperature is around 15.8 degree Celsius and the annual precipitation 1,240 millimeters. Within its jurisdiction are 16 districts and one county.
