# New Delhi

## meta info

- meta info version: 0.1
- country: India
- lattitude: 28.613889 
- longitude: 77.208889
- airport: Indira Gandhi International Airport (DEL)
- currency: Indian Rupee (INR)
- population: 22 million
- language: Hindi, Punjabi, Urdu
- time zone: UTC +5.30
- wiki: en.wikipedia.org/wiki/New_Delhi
- gallery: yes
- receiving partners: Xubo China
- tags: capital
- document created date: 2014-Oct-20
- document updated date: 2014-Oct-20

## city description

New Delhi Listeni /ˈnjuː dɛli/ is the capital of India and seat of the executive, legislative, and judiciary branches of the Government of India. It is also the centre of the Government of the National Capital Territory of Delhi. New Delhi is situated within the metropolis of Delhi and is one of the eleven districts of Delhi National Capital Territory.

With a population of 22 million in 2011, Delhi metropolitan region is the world's second most populous, the largest in India and also one of the largest in the world in terms of area. After Mumbai it is also the wealthiest city in India, and has the 2nd highest GDP of any city in South, West or Central Asia.
The foundation stone of the city was laid by George V, Emperor of India during the Delhi Durbar of 1911. It was designed by British architects, Sir Edwin Lutyens and Sir Herbert Baker. The new capital was inaugurated on 13 February 1931, by India's Viceroy Lord Irwin.