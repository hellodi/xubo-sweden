# zhengzhou

## meta info

- meta info version: 0.1
- country: China
- lattitude: 34.7466
- longitude: 113.625
- airport: Zhengzhou Xinzheng International Airport (CGO)
- currency: Renminbi (RMB)
- population: 11 million
- language: Chinese
- time zone: UTC +8
- wiki: en.wikipedia.org/wiki/Zhengzhou
- gallery: yes
- receiving partners: Xubo China
- tags: local capital
- document created date: 2014-Oct-12
- document updated date: 2014-Oct-12

## city description
With a long history, Zhengzhou is a national famous historical and cultural city ratified by the State Council. It was an important metropolis during the Shang dynasty as early as 3500 years ago and became a capital during the five dynasties of Xia, Shang, Guan, Zheng, and Han, and a prefecture during the eight dynasties of Sui, Tang, Five Dynasties, Song, Jin, Yuan, Ming, and Qing. The long history and brilliant civilization have made Zhengzhou rich in humanistic scenery, where there are 137 key protective units at national or provincial levels, including 26 units with 28 items of national key protective units. Places of interest with their unique charms are dawning interests and attentions of tourists from all over the world such as the Songshan Scenic Spots with Shoaling Temple, Central Holy Temple and Songshan National Forest Park as its highlight; the Yellow River Historical, Cultural and Relics Scenic Spots with the Yellow River Excursion Center, Yellow River Grand View and Dahe Village Primitive Village Remains; and Fuxi Mountain, Huancui Valley, North Song Dynasty's Imperial Tombs, North Wei's Grotto Temple, the birthplace of the
Chinese Forefather Emperor Huangdi and the birthplace of famous ancient poet
Dufu .In 1999, it was appraised in the first batch as one of the China's excellent tourism cities.

Every year, 9.02 million domestic tourists and 171 thousand overseas tourists will gather together in Zhengzhou to appreciate the historical sightseeing spots and culture there.

### Catering culture in Zhengzhou
The culinary of China is world wide famous, and as is known to all that in different places, we will have different ways to cook delicacy. In Zhengzhou, the most common food is pastry (cooked wheaten food), varies kinds of noodles and disserts made by wheat. The three essential factors, or key elements, by which Chinese cooking is judged are known as” color, aroma and taste”, so in Zhengzhou,people will make wheat into hundreds of food that tastes delicious. The “color” of Chinese food, the first of these elements which is so evident in a Chinese banquet, includes the layout and design of dishes, best exemplified in particular by the large elaborately-prepared cold dish served at the beginning of the dinner.”Aroma”implies more than what one’s nose can detect directly; you can always smell the fragrance of the food before they are on table!”Taste”is the art of proper seasoning, though it also involves the texture of food and fine slicing techniques. These three essential elements make food in Zhengzhou so tasteful and brilliant.

### Climate in Zheng Zhou
The green coverage of Zhengzhou is 33.8%，this is the main reason that Zhengzhou is called “The green city in the central plains”. Zhengzhou, with the mild climate and four distinct seasons has the annual average temperature of 14. 3 degree C. The monthly average temperature is about 27. 3 degree C in July. The coldest time in January, the temperature will drop down to -0.2 degree C.While thousands and millions flowers are in full blooming during the warm springs and cool autumns, you shouldn’t miss the best tourism season in Zhengzhou.

### Transportations in Zhengzhou
- Airport
Xinzheng airport is located 37 km southeast of Zhengzhou. It was opened in August 28, 1997, the 21st international airport in China. It was built to replace Dongjiao Airport, which lies in the immediate vicinity of downtown Zhengzhou.There are domestic and regional flights from the airport to most major cities in the People's Republic of China, international cargo flights to the Middle East and North Africa as well as charter flights to Thailand during the travel season.

- Bus
If you want to take buses in Zhengzhou, there will be two kinds of buses; one is the buses with air-conditioned facilities. It costs 2RMB for single journey; the other type is non-airconditioned which only needs 1RMB per time.

- Train
The distance from Shanghai to Zhengzhou is about 998km long and usually it will take about 7 hours to reach Zhengzhou. We recommend to book a ticket to
Zhengzhou and it will cost about 236.5RMB for the train ticket (The price is floating seasonally, but the price won’t be changed a lot.)
