from flask import Flask, jsonify, render_template, url_for, request, abort
from datetime import datetime
import glob
import string
import json
from pymongo import MongoClient
from flask.ext.misaka import markdown
from flask_mail import Message, Mail
import cloudinary.api
import cloudinary
import cloudinary.uploader
from cloudinary.utils import cloudinary_url
 
# cloudinary config
cloudinary.config(
  cloud_name = 'xubo-sweden',  
  api_key = '148369738863661',  
  api_secret = 'PA6dmUyTBx_44mru9z8CLDYnZKs'  
)
# Grab our connection information from the MONGOHQ_URL environment variable
# (mongodb://linus.compose.io:10045 -u username -pmy_password)
MONGO_URL = "mongodb://jd:12345678@linus.mongohq.com:10010/xubo_database"
client = MongoClient(MONGO_URL)
 
# Specify the database
db = client.xubo_database

app = Flask(__name__)

app.config.update(
    DEBUG=True
)

@app.route("/")
def home():
    page_info = {
        'title':'Programs',
        'nav_depth': [''],
        'title' : 'Home'
    }
    return render_template('home.html',page_info=page_info)

@app.route('/programs')
def programs():
    page_info = {
        'title':'Programs',
        'nav_depth': ['home', ''],
        'title' : 'Programs'
    }
    # Print a list of collections
    programs = db.programs
    return render_template('programs.html', 
                            page_info=page_info, 
                            programs=programs)

@app.route('/programs/<program_URI>')
def show_program(program_URI):
    picture_path_list = glob.glob("static/database/programs/" + program_URI + "/*.jpg")
    this_program = db.programs.find_one( { "program_URI":program_URI } )
    this_city = db.cities.find_one( { 'name': this_program['city_region'] } )
    this_program_info = {
        'name': string.capwords(this_program['name']),
        'country': this_program['country'],
        'city_region': this_program['city_region'],
        'city_URI': this_city['city_URI'],
        'description': markdown(this_program['description'],tables=True),
    }

    # Find the related participant info to show along with the story
    related_participant_info_list= []
    temp_URI = []
    participants_vist_this_city = db.participants.find( { "stories.visit_city" : this_program['city_region'] } )
    
    if participants_vist_this_city != None:
        for one_related_partipant in participants_vist_this_city:
            if not any(one_related_partipant['participant_URI'] in s for s in temp_URI): # remove duplication
                related_participant_info_list.append(one_related_partipant)
                temp_URI.append(one_related_partipant['participant_URI'])

    # Find the related program info to show along with the story
    related_program_info_list = []
    programs_in_this_city = db.programs.find( { "city_region" : this_program['city_region'] } )
    for one_related_program in programs_in_this_city:
        related_program_info_list.append(one_related_program)

    # set up page info
    page_info = {
        'title':'One destination',  
        'nav_depth': ['home', 'programs', ''],
        'title': this_program['name'].capitalize()
    }

    # finish and render
    return render_template('show_program.html', 
                            page_info=page_info,
                            picture_path_list=picture_path_list,
                            related_participant_info_list=related_participant_info_list,
                            related_program_info_list=related_program_info_list,
                            this_program_info=this_program_info)

@app.route('/destinations')
def destinations():
    page_info = {
        'title':'Destinations',
        'nav_depth': ['home', ''],
        'title' : 'Destinations'
    }

    # find the destinations and group them into countries
    cities = list(db.cities.find())
    city_URI_dictioinary = {}
    city_description = {}

    countries = list(db.countries.find())
    city_group_by_country = {}

    programs = list(db.programs.find())
    program_category_by_country = {}

    for country in countries:
        # found a new country in city list
        if not country['name'] in city_group_by_country:
            city_group_by_country[country['name']] = []
        # search for cities in this country
        for city in cities:
            city_description[city['name']] = city['description']
            if city['country'] == country['name']:
                city_group_by_country[country['name']].append(city['name'])
                city_URI_dictioinary[city['name']] = city['city_URI']

        # found a new country is program list
        if not country['name'] in program_category_by_country:
            program_category_by_country[country['name']] = []
        # search for programs in this country
        for program in programs:
            if (program['country'] == country['name']) and \
               (not program['primary_category'] in program_category_by_country[country['name']]):
                program_category_by_country[country['name']].append(program['primary_category'])

    # end of find cities

    # find thumbnails for cities
    city_thumbnail_public_ids = {}
    all_images = cloudinary.api.resources(tags=True, max_results=100)
    all_thumb_images = []
    for one_image in all_images['resources']:
        print one_image['public_id']
        if 'thumb' in one_image['tags']:
            all_thumb_images.append(one_image)

    for city in cities:
        for one_thumb_image in all_thumb_images:
            if city['name'].replace(' ', '-') in one_thumb_image['tags']:
                url, options = cloudinary_url(one_thumb_image['public_id'],
                            format = 'jpg',
                            width = 144,
                            height = 108,
                            crop = "thumb",
                        )
                city_thumbnail_public_ids[city['name']] = url

    # find thumbnails for countries
    country_thumbnail_public_ids = {}
    for country in countries:
        for one_thumb_image in all_thumb_images:
            if country['name'].replace(' ', '-') in one_thumb_image['public_id'] and country['continent'] in one_thumb_image['public_id']:
                url, options = cloudinary_url(one_thumb_image['public_id'],
                            format = 'jpg',
                            width = 400,
                            height = 300,
                            crop = "thumb",
                        )
                country_thumbnail_public_ids[country['name']] = url

    # find the meta info of a country
    country_meta = {}
    for country in countries:
        country_meta[country['name']] = {}
        country_meta[country['name']]['population'] = country['population'].title()
        if len(country['population']) > 1:
            country_meta[country['name']]['language'] = ', '.join(country['language']).title()
        else:
            country_meta[country['name']]['language'] = country['language'].capitalize()
        country_meta[country['name']]['currency'] = country['currency'].title()
        country_meta[country['name']]['time_zone'] = country['time_zone'].capitalize()

    # find all the country wide screen carousel pictures
    picture_path_list = []
    for one_image in all_images['resources']:
        if not 'thumb' in one_image['tags'] and 'country-image' in one_image['tags']:
            url, options = cloudinary_url(one_image['public_id'])
            picture_path_list.append(url)

    return render_template('destinations.html', 
                            page_info=page_info,
                            city_group_by_country=city_group_by_country,
                            city_description=city_description,
                            program_category_by_country=program_category_by_country,
                            all_cities = cities,
                            city_thumbnail_public_ids = city_thumbnail_public_ids,
                            country_thumbnail_public_ids = country_thumbnail_public_ids,
                            country_meta = country_meta,
                            picture_path_list=picture_path_list,
                            city_URI_dictioinary=city_URI_dictioinary,
                            destinations=destinations)

@app.route('/destinations/<destination_URI>')
def show_destination(destination_URI):
    this_destination = db.cities.find_one({"city_URI":destination_URI})

    this_destination_info = {
        'name': this_destination['name'],
        'country': this_destination['country'],
        'description': markdown(this_destination['description'],tables=True),
        'wiki': this_destination['wiki']
    }
    
    picture_path_list = []
    all_images = cloudinary.api.resources(tags=True, max_results=100)
    for image in all_images['resources']:
        if not 'thumb' in image['tags'] and this_destination['name'].replace(' ', '-') in image['tags'] and image['width'] == 1920 and image['height'] == 500:
            image_url, options = cloudinary_url(image['public_id'], format = 'jpg')
            picture_path_list.append(image_url)

    # Find the related participant info to show along with the story
    related_participant_info_list= []
    temp_URI = []
    participants_vist_this_city = db.participants.find( { "stories.visit_city" : this_destination['name'] } )
    
    if participants_vist_this_city != None:
        for one_related_partipant in participants_vist_this_city:
            if not any(one_related_partipant['participant_URI'] in s for s in temp_URI): # remove duplication
                related_participant_info_list.append(one_related_partipant)
                temp_URI.append(one_related_partipant['participant_URI'])

    # Find the related program info to show along with the story
    related_program_info_list = []
    programs_in_this_city = db.programs.find( { "city_region" : this_destination['name'] } )
    for one_related_program in programs_in_this_city:
        related_program_info_list.append(one_related_program)

    page_info = {
        'title':'One destination',  
        'nav_depth': ['home', 'destinations', ''],
        'title': this_destination['name'].capitalize() + ', ' + this_destination['country'].upper()
    }
    # finish and render
    return render_template('show_destination.html', 
                            page_info=page_info,
                            picture_path_list=picture_path_list,
                            related_participant_info_list=related_participant_info_list,
                            related_program_info_list=related_program_info_list,
                            this_destination_info=this_destination_info)

@app.route('/stories')
def stories():
    page_info = {
        'title':'Stories',
        'nav_depth': ['home', ''],
        'title' : 'Stories'
    }
    participants = db.participants.find()
    return render_template('stories.html', 
                            participants=participants,
                            page_info=page_info)

@app.route('/stories/<participant_URI>')
def show_story(participant_URI):
    # Find the participant using URI in database
    this_participant = db.participants.find_one({"participant_URI":participant_URI})

    # Find the participant's pictures in the database
    # The paths of individual pictures are stored in a list
    # later the list will be used in the template to get the link
    # of the pictures in a loop.
    gallery_path_list = []
    gallery_path_list.append(this_participant['stories'][0]['story_album_path'])

    # put all the necessary participant info in to a varible for the template
    this_story_info = {
        'participant_name': this_participant['name'],
        'participant_country': this_participant['country'],
        'visit_city': this_participant['stories'][0]['visit_city'],
        'visit_country': this_participant['stories'][0]['visit_country'],
        'story_text': markdown(this_participant['stories'][0]['story_text'],tables=True),
        'story_name': string.capwords(this_participant['stories'][0]['story_name']),
        'thumb_path': this_participant['thumb_150_path']
    }

    # Find the related participant info to show along with the story
    related_participant_info_list= []
    temp_URI = []
    participants_vist_same_city = db.participants.find( { "stories.visit_city" : this_participant['stories'][0]['visit_city'],
                                                          'participant_URI': {'$ne' : participant_URI}
                                                        } )
    if participants_vist_same_city != None:
        for one_related_partipant in participants_vist_same_city:
            if not any(one_related_partipant['participant_URI'] in s for s in temp_URI): # remove duplication
                related_participant_info_list.append(one_related_partipant)
                temp_URI.append(one_related_partipant['participant_URI'])

    participants_from_same_program = db.participants.find( { 'stories.program_primary_category' : this_participant['stories'][0]['program_primary_category'],
                                                             'participant_URI': {'$ne' : participant_URI }
                                                            })
    if participants_from_same_program != None:
        for one_related_partipant in participants_from_same_program:
            if not any(one_related_partipant['participant_URI'] in s for s in temp_URI): # remove duplication
                related_participant_info_list.append(one_related_partipant)
                temp_URI.append(one_related_partipant['participant_URI'])

    # Find the related program info to show along with the story
    related_program_info_list = []
    programs_in_same_city = db.programs.find( { "city_region" : this_participant['stories'][0]['visit_city'] } )
    for one_related_program in programs_in_same_city:
        related_program_info_list.append(one_related_program)

    # Set the page info
    page_info = {
        'title':'One story',
        'nav_depth': [ "home", 'stories', ''],
        'title': this_story_info['participant_name'].capitalize() + "\'s story in " + this_story_info['visit_city'].capitalize()
    }

    # Render the template
    return render_template('show_story.html', 
                            page_info=page_info,
                            gallery_path_list=gallery_path_list,
                            this_story_info=this_story_info,
                            related_participant_info_list=related_participant_info_list[:3],
                            related_program_info_list=related_program_info_list[:3])

@app.route('/about-us')
def about_us():
    page_info = {
        'title':'About Us',
        'nav_depth': ['home', ''],
    }
    return render_template('about_us.html', page_info=page_info)

@app.route('/single-contact-form')
def single_contact_form():
    return render_template('single-contact-form.html')

@app.route('/-contact-us')
def _contact_us():
    contact_request = {}
    contact_request['name'] = request.args.get('name','')
    contact_request['email'] = request.args.get('email', '')
    contact_request['message'] = request.args.get('message', '')
    contact_request['time_stamp'] = datetime.now()
    contact_request['remote_ip'] = request.remote_addr
    contact_request['user_agent'] = request.headers.get('User_Agent')
    contact_request['platform'] = request.user_agent.platform
    contact_request['browser'] = request.user_agent.browser
    contact_request['browser_version'] = request.user_agent.version
    contact_request['language'] = request.user_agent.language
    contact_request['processed'] = False
    db.visitor_contact_messages.insert(contact_request)
    #return abort(500)
    return json.dumps({'status': 'ok', 'Access-Control-Allow-Credentials': 'true'})

if __name__ == "__main__":
    app.run(debug=True)
