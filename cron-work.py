# Import smtplib for the actual sending function
import mandrill
import schedule
import time 
import datetime
import urllib2
from time import strftime
import os
import pymongo
from pymongo import MongoClient
import re

def read_heroku():
	print "reading heroku...",
	print time.strftime("%Y-%m-%d %H:%M:%S"),
	urllib2.urlopen("http://guarded-harbor-6399.herokuapp.com/").read()
	print "done"

def check_visitor_message():

	# Grab our connection information from the MONGOHQ_URL environment variable
	# (mongodb://linus.compose.io:10045 -u username -pmy_password)
	MONGO_URL = "mongodb://jd:12345678@linus.mongohq.com:10010/xubo_database"
	#MONGO_URL = "mongodb://'tester':'password'@ds033400.mongolab.com:33400/bnsl-fika-db"
	#connection = Connection(MONGO_URL)
	client = MongoClient(MONGO_URL)
	 
	# Specify the database
	db = client.xubo_database
	unprocessed_messages = db.visitor_contact_messages.find( { "processed" : False } )
	print 'We have ' + str(unprocessed_messages.count()) + ' unprocessed messages.'
	for one_message in unprocessed_messages:
		print 'Processing: ' + one_message['email']
		if not re.match(r"[^@]+@[^@]+\.[^@]+", one_message['email']):
			print 'Invalid email address: ' + one_message['email']
			db.visitor_contact_messages.update({'_id':one_message['_id']}, 
										   {"$set": {
										       'processed': True, 
										       'invalid_email': True
										       }
										   }, 
										   upsert=False)
		send_email(one_message['email'], one_message['name'], one_message['message'])
		one_message['processed'] = True
		db.visitor_contact_messages.update({'_id':one_message['_id']}, 
										   {"$set": 
										       {'processed': True} 
										   }, 
										   upsert=False)
		print 'Done.'

	return True

def send_email(visitor_email, visitor_name, message_content):
	try:
		mandrill_client = mandrill.Mandrill('n-hwjZsm1MS4CKpegjHHNg')
		message = {
		'auto_html': None,
		'auto_text': None,
		'from_email': 'sweden@xubo.com',
		'from_name': 'Xubo Sweden',
		'global_merge_vars': [{'content': 'merge1 content', 'name': 'merge1'}],
		'google_analytics_campaign': 'message.from_email@example.com',
		'google_analytics_domains': ['example.com'],
		'headers': {'Reply-To': 'sweden@xubo.org'},
		'html': '<p>' + message_content + '</p>',
		'important': False,
		'inline_css': None,
		'metadata': {'website': 'www.xubo.org'},
		'preserve_recipients': None,
		'recipient_metadata': [{'rcpt': visitor_email,
		'values': {'user_id': 123456}}],
		'return_path_domain': None,
		'signing_domain': None,
		'subject': 'Contact request from ' + visitor_email,
		'tags': ['test-resets'],
		'text': message_content,
		'to': [{'email': visitor_email,
		'name': visitor_name,
		'type': 'to'}],
		'track_clicks': None,
		'track_opens': None,
		'tracking_domain': None,
		'url_strip_qs': None,
		'view_content_link': None
		}

		result = mandrill_client.messages.send(message=message, async=False, ip_pool='Main Pool')
		
		'''
		[{'_id': 'abc123abc123abc123abc123abc123',
		'email': 'jd.regreg@example.com',
		'reject_reason': 'hard-bounce',
		'status': 'sent'}]
		'''
		print result

	except mandrill.Error, e:
	    # Mandrill errors are thrown as exceptions
	    print 'A mandrill error occurred: %s - %s' % (e.__class__, e)
	    # A mandrill error occurred: <class 'mandrill.UnknownSubaccountError'> - No subaccount exists with the id 'customer-123'    
	    raise

	return True

schedule.every(30).minutes.do(read_heroku)
schedule.every(30).seconds.do(check_visitor_message)

while True:
    schedule.run_pending()
    time.sleep(1)

